/**
 * config.js
 * =========
 *
 * Wszystkie opcje konfiguracyjne dla aplikacji.
 * UWAGA: ten plik musi być załadowany na początku.z
 */

(function () {

"use strict";

var civapp = {};

civapp.conf = {

  // Bazowy url dla całej aplikacji.
  // Zmień na taki, z którego korzystasz.

  baseUrl: 'http://84.10.12.178:8888',

  facebook: {
    appId: '358632074259188',
    appName: 'Login Test',
    scope: ["email"],
    redirectUri: 'https://www.facebook.com/connect/login_success.html'
  }
};

window.civapp = civapp;

})();
