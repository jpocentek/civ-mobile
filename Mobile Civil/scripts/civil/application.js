/**
 * application.js
 * ==============
 *
 * Rdzeń całego programu, aka główny kontroler. Tutaj dopisujemy wszystkie
 * metody, które chcemy później zastosować w widokach.
 */

(function ($, _, kendo, civapp) {

"use strict";

// WAŻNE! - inicjalizacja aplikacji Kendo

function initKendo () {
  return new kendo.mobile.Application(document.body, {
    transition: 'slide'
  });
}

var kendoApp = initKendo();

// Czyścimy niepotrzebne już formularze i inne elementy

function cleanup () {
  civapp.fillUserPanel();
  civapp.auth.setToken();
}

_.extend(civapp, {

  // remember to adjust accordingly!!!

  version: '0.0.1',

  // Sprawdzamy, czy skrypty FB już się załadowały

  fbState: false,

  initialize: function () {

    // Plugin facebooka - w browserze musi być zainicjowany
    this.fbInit = function () {
      if (window.cordova.platformId == "browser" && !civapp.fbState) {
        facebookConnectPlugin.browserInit(civapp.conf.facebook.appId);
        civapp.fbState = true;
      }
    };

    this.router = new kendo.Router({
      // Change tutaj działa jak middleware - możemy dodawać
      // wszystkie elementy wspólne dla całej aplikacji
      change: function(e) { 
        cleanup();
      }
    });

    this.router.start();

    // alias dla łatwiejszego dostępu do kendo Application
    // poprzez interfejs naszej civapp aplikacji.

    this.kendo = kendoApp;

    this.commentForm = null;

    $(document).on('click', '.toggle-comments', function (e) {
      var id = $(e.currentTarget).attr('data-id');
      var ct = $(e.currentTarget).attr('data-ct');
      var parent = $(e.currentTarget).attr('data-parent');
      if (_.isNull(civapp.commentForm)) {
        civapp.commentForm = new civapp.comments.CommentForm(id, ct, parent);
      } else {
        civapp.commentForm.update(id, ct, parent);
      }
      civapp.commentForm.show();
    });
  },

  // Sprawdzamy, czy użytkownik jest zalogowany kiedy uruchamia aplikację.

  authCheck: function (e) {
    if (civapp.auth.isAuthorized()) {
      kendoApp.replace('views/userspace.html');
      civapp.fillUserPanel ();
    }
  },

  // Obsługa formularza do logowania użytkownika

  initLoginForm: function (e) {
    $('#login-form [type="submit"]').on('click', function (e) {
      $('#login-form').submit();
    });

    $('#login-form').on('submit', function (e) {
      e.preventDefault();
      var email = $('#login-email').val(),
          password = $('#login-password').val();
      civapp.auth.login(email, password);
    });
  },

  // Wylogowanie użytkownika

  logoutUser: function () {
    civapp.auth.logout();
  },

  // Wypełniamy dane użytkownika w panelu bocznym

  fillUserPanel: function () {
    if (!_.isUndefined($('#user-main-data').data('civpanel')))
      return true;
    var data = civapp.storage.getUserData();
    if (_.isNull(data))
      return true;
    $('#user-avatar').attr('src', ([civapp.conf.baseUrl, data.avatar]).join(''));
    $('#user-full-name')
      .html(([data.first_name, data.last_name]).join(' '));
    $('#user-main-data').data('civpanel', data);
  }

});

})(jQuery, _, kendo, civapp);
