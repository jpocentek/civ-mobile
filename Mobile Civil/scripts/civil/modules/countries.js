/**
 * countries.js
 * ============
 *
 * Widoki i akcje powiązane z krajami. To najprostszy model,
 * jaki mamy, dlatego najłatwiej na nim testować.
 */

(function ($, kendo, civapp) {

  "use strict";

  civapp.countries = {

    // Lista wszystkich krajów - głównie do testowania prostych widoków

    countryList: function (e) {

      var datasource = civapp.db.countries();

      $("#listview").kendoMobileListView({
        dataSource: datasource,
        pullToRefresh: true,
        template: $('#country-entry-tpl').html()
      });
    }
  };

})(jQuery, kendo, civapp);