//
// search.js
// =========

(function ($, civapp, _) {

  "use strict";

  // Obsługiwane typy zawartości. Pozwalają nam pobrać
  // z serwera listę typów zawartośći i filtrować wyniki
  // wyszukiwania z uwzględnieniem modelu.
  // Patrz opis przy: /api-core/content_types/

  var CONTENT_TYPES = [
    {'app': 'locations', 'model': 'location'},
    {'app': 'blog', 'model': 'news'},
    {'app': 'ideas', 'model': 'idea'},
    {'app': 'polls', 'model': 'poll'},
    {'app': 'topics', 'model': 'discussion'}
  ];

  // Funkcja pobiera listę używanych typów zawartości
  // i przekazuje je do funkcji, którą podajemy jako parametr.

  function getContentTypes (fn) {
    $.get(([civapp.conf.baseUrl, '/api-core/content_types/']).join(''),
      function (response) {
        var ctList = [], check;
        _.each(response, function (item) {
          check = _.findWhere(CONTENT_TYPES, {
            app: item.app_label,
            model: item.model
          });
          if (!_.isUndefined(check)) {
            ctList.push(item);
          }
        });

        if (_.isFunction(fn)) {
          fn (ctList);
        }
      }
    );
  }

  civapp.search = {

    form: null,

    view: null,

    // Inicjalizujemy formularz i uruchamiamy przechwytywanie
    // zdarzeń. Ta funkcja powinna być wywołana po wyświetleniu
    // widoku (data-after-show).

    initSearchForm: function (e) {

      // Formularz został już zainicjalizowany. Nie
      // bardzo mamy jak to sprawdzić bez DOM ready,
      // więc kontrolujemy stan przez zmienną
      if (!_.isNull(civapp.search.form)) {
        return true;
      }

      var $form = $(document.createElement('div'));
      var tpl = kendo.template($('#model-input-tpl').html());
      $form.html($('#search-form-tpl').html())
        .insertBefore('#result-list')
        .on('submit', function (e) {
          e.preventDefault();
          civapp.search.search();
        });
      civapp.search.form = $form;

      // Uzupełniamy formularz checkboxami
      // z typami zawartości do filtrowania
      getContentTypes (function (ctList) {
        _.each(ctList, function (ct) {
          $('#model_list').append(tpl(ct));
        });
      });
    },

    // Inicjalizujemy okno wyświetlające wyniki i upewniamy
    // się, że zostanie poprawnie zresetowane po wpisaniu
    // nowej frazy. Ta funkcja powinna być wywołana po wyświetleniu
    // widoku (data-after-show).

    initResultWindow: function (datasource) {
      if (!_.isNull(civapp.search.view)) {
        civapp.utils.resetWindow($('#result-list'));
      }
      $('#result-list').kendoMobileListView({
        dataSource: datasource,
        endlessScroll: true,
        template: $('#result-tpl').html()
      });
      civapp.search.view = $('#result-list')
        .data('kendoMobileListView');
    },

    // Metoda, która przeprowadza właściwe wyszukiwanie.

    search: function () {
      var query = $('#q').val();
      var ctList = []

      $('[name="ct-list-choice"]:checked').each(
        function () {
          ctList.push($(this).val());
        }
      );

      // Bez sensu odpowiadać na puste zapytania
      if (query.length === 0) {
        return false;
      }

      var datasource = civapp.db.search(query, ctList);

      civapp.search.initResultWindow(datasource);
    }
  };

})(jQuery, civapp, _);