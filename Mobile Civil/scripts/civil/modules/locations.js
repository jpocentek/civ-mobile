/**
 * locations.js
 * ============
 *
 * Podsumowanie lokalizacji. Prosty baner z info o lokalizacji
 * oraz zestaw funkcji pozwalających pobierać/filtrować zawartość
 * w/g typu.
 */

(function ($, _, kendo, civapp) {

  "use strict";

  civapp.locations = {

    // Przeładowanie zawartości okna (kolekcji, np. po przefiltrowaniu).
    //
    // @param { Number } Id lokalizacji
    // @param { String } Nazwa typu zawartości

    _loadContent: function (id, content) {
      var datasource = civapp.db.placecontents(id, content);

      civapp.utils.resetWindow($('#contents'));

      $('#contents').kendoMobileListView({
        dataSource: datasource,
        endlessScroll: true,
        template: $('#pcontent-tpl').html()
      });
    },

    // Tworzy listę menu z filtrami dla typów zawartości
    //
    // @param { Number } Id lokalizacji
    // @param { String } Nazwa typu zawartości

    _createMenu: function (id, ct) {
      var $el = $('#placenav');
      var tpl = kendo.template($('#menu-entry-tpl').html());
      var ctList = ['all', 'news', 'idea', 'discussion', 'poll'];

      ct = ct || 'all';

      // Nie twórz nowego menu, jeżeli wracamy do poprzedniego widoku

      if ($el.data('civMenu') === id)
        return true;
      $el.empty().data('civMenu', {id: id});

      // Aktualnie wybrany element otrzyma klasę 'active'

      _.map(ctList, function (item) {
        var attrs = {
          id   : id, ct: item,
          label: civapp.utils.capFirst(item),
          cls  : ""
        };

        if (item === ct)
          attrs.cls = ' active';

        $el.append(tpl(attrs));
      });

      $('.filter-menu-link').on('click',
        function (e) {
          e.preventDefault();
          civapp.locations._loadContent(
            $(this).attr('data-id'),
            $(this).attr('data-ct'));
          $el.find('.filter-menu-link')
            .removeClass('active');
          $(this).addClass('active');
        }
      );
    },

    // Podstawowy widok inicjalizujący lokalizację

    locationDetails: function (e) {
      var url = ([civapp.conf.baseUrl, '/api-locations/locations/',
                                  e.view.params.id, '/']).join('');
      var tpl = kendo.template($('#location-tpl').html());
      var menu = civapp.locations
        ._createMenu(e.view.params.id, e.view.params.ct);

      civapp.locations._loadContent(e.view.params.id, e.view.params.ct);

      $.get(url, function (response) {
        $('#location-summary').html(tpl(response));
      });
    }
  };

})(jQuery, _, kendo, civapp);