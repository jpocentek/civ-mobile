//
// polls.js
// ========

(function ($) {
  
"use strict";

civapp.polls = {

  // Widok pojedynczej dyskusji

  pollDetail: function (e) {
    var url = ([civapp.conf.baseUrl, '/rest/polls/', e.view.params.id, '/']).join(''),
        tpl = kendo.template($('#poll-entry-detail-tpl').html());

    $.get(url, function (response) {
      if (response.image) {
        response.image = ([civapp.conf.baseUrl, response.image]).join('');
      }
      $('#poll-content')
        .html(tpl(response));
    });
  }
};

})(jQuery);