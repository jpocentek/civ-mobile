//
// topics.js
// =========

(function ($) {
  
"use strict";

civapp.topics = {

  // Widok pojedynczej dyskusji

  topicDetail: function (e) {
    var url = ([civapp.conf.baseUrl, '/rest/topics/', e.view.params.id, '/']).join(''),
        tpl = kendo.template($('#topic-entry-detail-tpl').html());

    $.get(url, function (response) {
      if (response.image) {
        response.image = ([civapp.conf.baseUrl, response.image]).join('');
      }
      $('#topic-content')
        .html(tpl(response));
    });
  }
};

})(jQuery);