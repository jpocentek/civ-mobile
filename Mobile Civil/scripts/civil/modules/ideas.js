/**
 * ideas.js
 * ========
 *
 * Widoki i akcje powiązane z ideami.
 */

(function ($, kendo, civapp) {

"use strict";

civapp.ideas = {

  // Widok pojedynczej idei

  ideaDetail: function (e) {
    var url = ([civapp.conf.baseUrl, '/rest/ideas/', e.view.params.id, '/']).join(''),
        tpl = kendo.template($('#content-entry-details').html());

    $.get(url, function (response) {
      response.image = ([
        civapp.conf.baseUrl,
        response.image]).join('');

      $('#main-content')
        .html(tpl(response));
    });
  },

  // Głosowanie na idee

  voteIdea: function (idea, vote) {
    var url = ([civapp.conf.baseUrl, '/api-ideas/votes/']).join(''),
        post = {idea: idea, vote: vote,
                user: civapp.storage.getUserData('id')};
    civapp.utils.postOrAlert(url, post,
      function (data) {
        civapp.ideas.updateCounter(vote);
      }
    );
  },

  voteUp: function (e) {
    return civapp.ideas.voteIdea($('#idea-id').val(), true);
  },

  voteDown: function (e) {
    return civapp.ideas.voteIdea($('#idea-id').val(), false);
  },

  // Zmienia podsumowanie liczby głosów po oddaniu głosu

  updateCounter: function (vote) {
    var $counter = $('#total-votes');
    var start = parseInt($counter.val(), 10);
    if (isNaN(start))
      start = 0;
    $counter.val(vote ? ++start : --start);
  }
};

})(jQuery, kendo, civapp);