//
// comments.js
// ===========
//
// Obsługa komentarzy.

(function ($, _, kendo, civapp, moment) {

"use strict";

// Helper, który podnosi liczbę głosów.
//
// @param { jQuery DOM Element } Element, który służy za "counter"

function updateCounter ($count) {
  var start = parseInt($count.text(), 10);
  if (isNaN(start))
    start = 0;
  $count.text(start + 1);
}

// Forma do komentarzy
//
// @param { Number } Id komentowanego obiektu
// @param { Number } Id typu zawartości
// @param { Number } Id komentarza jeżeli odpowiadamy (opcjonalny)

function CommentForm (id, ct, parentId) {
  this.el = $('#comment-form-tpl').html();
  this.$el = $($.parseHTML(this.el));

  this.update(id, ct, parentId);

  // Disable standard submit functionality

  this.$el.find('form:first').on('submit',
    function (e) {
      e.preventDefault();
      return false;
    }
  );

  // Submit form when button is clicked

  this.$el.find('[type="submit"]').on('click',
    function (e) {
      e.preventDefault();
      this.submit();
    }
  .bind(this));

  this.$el.find('.close').on('click',
    function (e) {
      e.preventDefault();
      this.hide();
    }
  .bind(this));

  $('footer').empty().append(this.$el);

  this.$el.data('civilform', this);
}

// Pobranie zawartości formularza i serializacja dla POST

CommentForm.prototype._getData = function () {
  var data = _.clone(this.data);
  data.comment = this.$el.find('#comment-content').val();
  data.submit_date = moment().format();
  if (_.isUndefined(data.parent)) {
    delete data.parent;
  }
  return data;
};

// Walidacj i utworzenie komentarza

CommentForm.prototype.submit = function () {
  var _self = this,
      data = _self._getData(),
      url = ([civapp.conf.baseUrl, '/rest/comments/']).join('');

  civapp.utils.postOrAlert(url, data,
    function (response) {
      alert("Comment saved!");
      updateCounter($('#comment-count'));
      _self.hide();
    }
  );
};

CommentForm.prototype.update = function (id, ct, parent) {
  this.data = {content_id: id, content_type: ct, parent: parent};
};

CommentForm.prototype.show = function () {
  this.$el
    .show()
    .find('#comment-content')
      .val('')
      .focus();
};

CommentForm.prototype.hide = function () {
  this.$el.hide();
};

// FIXME: zombiesy tutaj prawdopodobnie zostają

CommentForm.prototype.destroy = function () {
  this.$el.find('[type="submit"]').off('click');
  this.$el.find('.close').off('click');
  this.$el.empty().remove();
  $('#comment-main-form').empty().remove();
};

// Główny moduł komentarzy

civapp.comments = {

  // Pobiera listę komentarzy powiązanych z obiektem

  getComments: function (e) {
    var datasource = civapp.db.comments(
      e.view.params.id,
      e.view.params.ct,
      e.view.params.cl
    );

    civapp.utils.resetWindow($('#commentlist'));

    $('#commentlist').kendoMobileListView({
      dataSource: datasource,
      endlessScroll: true,
      template: $('#comment-tpl').html()
    });
  },

  // Pobiera listę odpowiedzi do komentarza

  getReplies: function (e) {
    var url = ([civapp.conf.baseUrl, '/rest/comments/',
                e.view.params.id, '/replies/']).join('');
    $.get(url, function (data) {
      $('#reply-list').empty().kendoMobileListView({
        dataSource: data,
        template: $('#comment-tpl').html()
      });
    });
  },

  // Tworzy formularz do komentowania przypasowany do elementu

  CommentForm: CommentForm
};

})(jQuery, _, kendo, civapp, moment);