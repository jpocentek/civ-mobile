//
// blog.js
// =======

(function ($) {

"use strict";

civapp.blog = {

  // Widok pojedynczego wpisu

  newsDetail: function (e) {
    var url = ([civapp.conf.baseUrl, '/rest/news/', e.view.params.id, '/']).join(''),
        tpl = kendo.template($('#news-entry-detail-tpl').html());

    $.get(url, function (response) {
      response.image = ([
        civapp.conf.baseUrl,
        response.image
      ]).join('');
      $('#news-content')
        .html(tpl(response));
    });
  }
};

})(jQuery);