/**
 * userspace.js
 * ============
 *
 * Widoki i akcje powiązane z użytkownikiem.
 */

(function ($, civapp) {

  "use strict";

  civapp.userspace = {

    // Strona główna dla użytkownika - podsumowanie aktywności w jego lokalizacjach

    initUserContent: function (e) {

      var datasource = civapp.db.usercontents();

      $('#content-list').kendoMobileListView({
        dataSource: datasource,
        endlessScroll: true,
        template: $('#ucontent-tpl').html()
      });
    },

    // Po wywołaniu strony głównej, sprawdzamy, czy użytkownik jest zalogowany

    checkCredentials: function (e) {

      if (civapp.auth.isAuthorized()) {
        // FIXME: takie proste sprawdzenie, czy content
        // jest "zassany", może nie wystarczyć.
        var check = $('#content-list > li').length;
        if (check <= 0) {
          civapp.userspace.initUserContent();
        }
      }
    },

    // Widok "moje lokalizacje"

    listMyLocations: function (e) {

      var datasource = civapp.db.userlocations();

      $('#my-location-list').kendoMobileListView({
        dataSource: datasource,
        template: $('#my-location-tpl').html()
      });
    }
  };

})(jQuery, civapp);