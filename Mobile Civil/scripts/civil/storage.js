/**
 * storage.js
 * ==========
 *
 * Narzędzia do komunikacji z magazynem urządzenia (localStorage lub inne
 * rozwiązanie, jeżeli to nie jest dostępne). Kontroluje informacje od aplikacji
 * zapisywane na / wczytywane z urządzenia.
 */

(function (_, civapp) {

"use strct";

var storage = {

  backend: null,

  // W zamierzeniu sprawdzamy obsługę localStorage na
  // urządzeniu i ew. zastępujemy innym rozwiązaniem.
  // Na razie localStorage wystarcza.

  initialize: function () {
    this.backend = window.localStorage;
  },

  // Zwraca pojedynczą zawartość schowka w/g klucza

  getItem: function (item) {
    return this.backend.getItem(item);
  },

  // Ustawia pojedynczą wartość

  setItem: function (item, value) {
    return this.backend.setItem(item, value);
  },

  // Usuwa pojedynczą wartość

  removeItem: function (item) {
    this.backend.removeItem(item);
  },

  // Wczytujemy obiekt z locale storage i
  // konwertujemy na obiekt JS

  loadDict: function (dict) {
    var itm = this.getItem(dict);
    if (!_.isNull(itm)) {
      return JSON.parse(itm);
    }
    return null;
  },

  // Konwertujemy obiekt JS na string
  // i zapisujemy w magazynie

  saveDict: function (dict, items) {
    var itm = this.getItem(dict) || {};
    if (!_.isEmpty(itm)) {
      itm = JSON.parse(itm);
    }
    _.extend(itm, items);
    this.setItem(dict, JSON.stringify(itm));
  },

  // Pobiera dane użytkownika z magazynu urządzenia.
  // Zwraca null jeżeli klucz nie ma wartości albo
  // użytkownik nie jest zalogowany.
  //
  // @param { String } Klucz ze słownika (opcjonalny)

  getUserData: function (key) {
    var userDict = this.loadDict('userdata');
    if (!_.isUndefined(key) && !_.isNull(userDict)) {
      return userDict[key];
    } else {
      return userDict;
    }
  }
};

storage.initialize();

civapp.storage = storage;

})(_, civapp);