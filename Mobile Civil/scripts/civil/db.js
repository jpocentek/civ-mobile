/**
 * data-provider.js
 * ================
 *
 */

(function ($, _, kendo, civapp) {

  "use strict";

  // Prosty wrapper na KendoSource, który montuje
  // nam datasource obsługujące standardową paginację
  //
  // @param { string } Adres URL dla "kolekcji"
  // @returns { kendo.DataSource }

  function paginatedDataSource (url) {
    return new kendo.data.DataSource({
      transport: {
        read: {
          url: url,
          datatype: "json",
          contentType: "application/json"
        }
      },

      pageSize: 50,
      serverPaging: true,

      schema: {
        data: 'results',
        total: 'count'
      }
    });
  }

  civapp.db = {

    // Lista krajów - do testowania CRUDa

    countries: function () {
      return new kendo.data.DataSource({
        transport: {
          read: { 
            url: ([civapp.conf.baseUrl, '/api-locations/countries/']).join(''),
            datatype: "json",
            contentType: "application/json"
          }
        }
      });
    },

    // Podsumowanie aktywności w miejscach obserwowanych przez użytkownika

    usercontents: function () {
      var url = ([civapp.conf.baseUrl, '/api-userspace/contents/']).join('');
      return paginatedDataSource(url);
    },

    // Lista miejsc obserwowanych przez użytkownika

    userlocations: function () {
      return new kendo.data.DataSource({
        transport: {
          read: { 
            url: ([civapp.conf.baseUrl, '/api-userspace/locations/']).join(''),
            datatype: "json",
            contentType: "application/json"
          }
        }
      });
    },

    // Podsumowanie aktywności w lokalizacji

    placecontents: function (id, ct) {
      var content = ct || 'all';
      var url = ([civapp.conf.baseUrl, '/api-locations/contents/?pk=',
                  id, '&content=', content]).join('');
      return paginatedDataSource(url);
    },

    // Lista komentarzy dla obiektu
    //
    // @param id { Number } Id obiektu
    // @param ct { Number } Id typu zawartości
    // @param cl { String } Nazwa typu zawartości
    //
    // @returns { kendo.data.DataSource }
    //
    // FIXME: Po co typ i nazwa? Jedno powinno wystarczyć

    comments: function (id, ct, cl) {
      var url = ([civapp.conf.baseUrl, '/rest/comments/?content-label=',
                  cl, '&content-type=', ct, '&content-id=', id]).join('');
      return paginatedDataSource(url);
    },

    // Wyszukiwarka
    //
    // @param { string } Wyszukiwana fraza (oczywiście url-encoded)
    // @param { Array }  Lista ID typów zawartości
    // @returns { kendo.DataSource }

    search: function (query, ctList) {
      var url = ([civapp.conf.baseUrl, '/api-core/search/?q=',
                  encodeURIComponent(query)]).join('');
      if (ctList !== undefined) {
        url += '&ct=' + ctList.join(',');
      }
      return paginatedDataSource(url);
    }
  };

})(jQuery, _, kendo, civapp);