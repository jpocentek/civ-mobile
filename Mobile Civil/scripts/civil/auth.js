//
// auth.js
// =======
//
// Moduł odpowiedzialny za uwierzytelnianie/wylogowanie użytkownika
// oraz kontrolowanie jego aktywności na stronie. W praktyce wszystko, co
// jest powiązane z użytkownikiem.

(function ($, _, civapp) {

"use strict";

civapp.auth = {

  url: ([civapp.conf.baseUrl, '/api-userspace']).join(''),

  // Autoryzujemy użytkownika i zapisujemy jego dane. Wywołaj tę funkcję
  // po zalogowaniu, obojętnie w jaki sposób, podając odpowiedź serwera.
  //
  // @param { plain object }  Server response

  authenticate: function (response) {
    if (response.success) {
      civapp.fillUserPanel ();
      civapp.storage.saveDict('userdata', response);
      civapp.kendo.replace('views/userspace.html');
    } else {
      alert("Invalid login credentials");
    }
  },

  // Logowanie użytkownika (przez Civil Hub, czyli normalnie)
  //
  // @param {string}   Email użytkownika
  // @param {string}   Hasło użytkownika
  // @param {function} Callback function
  //
  // @returns {string | boolean} Token użytkownika albo false

  login: function (email, password, fn) {
    var url = ([this.url, '/api-token-auth/']).join('');
    $.post(url, {email: email, password: password},
      function (response) {
        civapp.auth.authenticate(response);
      }
    );
  },

  // Wylogowanie użytkownika

  logout: function () {
    civapp.storage.removeItem('userdata');
    civapp.kendo.replace('index.html');
  },

  // Sprawdzamy, czy użytkownik jest zalogowany
  //
  // @returns { boolean }

  isAuthorized: function () {
    return civapp.storage.getItem('userdata') !== null;
  },

  // Ustawiamy token uwierzytelniający zapytania POST, PUT i DELETE

  setToken: function () {
    if (this.isAuthorized()) {
      var token = (["Token", civapp.storage.getUserData('token')]).join(' ');
      $.ajaxSetup({
        beforeSend: function (request) {
          request.setRequestHeader("Authorization", token);
        }
      });
    }
  },

  // Logowanie przez Facebook
    
  fbLogin: function () {
    var facebookConfig = {
      name: 'Facebook',
      loginMethodName: 'loginWithFacebook',
      endpoint: 'https://www.facebook.com/dialog/oauth',
      response_type: 'token',
      client_id: civapp.conf.facebook.appId,
      redirect_uri: civapp.conf.facebook.redirectUri,
      access_type: 'online',
      scope: civapp.conf.facebook.scope,
      display: 'touch'
    };
    var facebook = new IdentityProvider(facebookConfig);
    facebook.getAccessToken(function (token) {
      // Testujemy tokeny i uwierzytelnianie
      $.post(([civapp.conf.baseUrl, '/api-core/token_check/']).join(''), {token: token}, function (resp) {
        console.log(resp);
      });
    });
  },

  // Sprawdzenie statusu na FB

  fbCheckout: function () {
    civapp.fbInit();
    facebookConnectPlugin.getLoginStatus( 
      function (response) { alert(JSON.stringify(response)) },
      function (response) { alert(JSON.stringify(response)) });
  }
};

})(jQuery, _, civapp);
