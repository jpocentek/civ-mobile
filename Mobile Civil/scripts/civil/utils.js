//
// utils.js
// ========
//
// Różne helpery i narzędzia do ogólnego zastosowania.
//

(function ($, _, kendo, civapp) {

"use strict";

civapp.utils = {

  // Tworzy menu ze zdefiniowanymi linkami
  //
  // @param { jQuery DOMElement } Element, do którego "przyczepiamy" menu
  // @param { array } Linki w fomacie [{ label:"label",action:"action" }, ... ]
  //
  // @returns { jQuery DOMElement } Kompletne menu

  createMenu: function ($el, options) {
    var tpl = kendo.template('<a href="#: action #">#: label #</a>');
    $el.empty();
    _.each(options, function (item) {
      $el.append(tpl(item));
    });
    $el.kendoMobileTabStrip();
    return $el;
  },

  // Skrót dla POST AJAX. Niektóre widoki REST z założenia zwracają response
  // inny niż 200 (np. w przypadku dodawania obiektu). Wtedy błędy walidacji
  // mamy w polach "non_field_errors" itp.
  //
  // @param { string } Url
  // @param { object } Dane do wysłania POST-em
  // @param { func }   Success callback (data will be passed here)

  postOrAlert: function (url, data, fn) {
    $.ajax({
      type: 'POST',
      data: data,
      url: url,
      success: function () {fn (data);},
      error: function (err) {
        var msg = [];
        var res = JSON.parse(err.responseText);
        _.each(_.keys(res), function (key) {
          var lb = civapp.utils.capFirst(key.replace(/_/g, ' '));
          var ct = _.isArray(res[key]) ? res[key].join(', ')
                                       : res[key];
          msg.push(([lb, ": ", ct]).join(''));
        });
        alert (msg);
      }
    });
  },

  // Zamień pierwszą literę słowa na wielką
  //
  // @param   { String } Słowo
  // @returns { String }

  capFirst: function (word) {
    return word[0].toUpperCase() + word.slice(1);
  },

  // Czyścimy widoki powiązane z listview i resetujemy scroller.
  // Sczerze - nie mam pojęcia, jak zrobić to lepiej.
  //
  // @param { jQuery DOM Element } Element powiązany z kendoMobileListView

  resetWindow: function ($el) {
    var id = $el.attr('id') || "auto-listview";
    var kv = $el.data('kendoMobileListView');

    if (_.isUndefined(kv))
      return

    kv.scroller().reset();
    kv.destroy();

    // I tak na wszelki:

    $el.replaceWith((['<ul id="', id, '"></ul>']).join(''));
  },

  // Skrót, który dodaje podany suffix do url ustawionego w konfiguracji.
  //
  // @param   { string } Parametry do adresu URL (wszystko, co jest po nazwie serwera)
  // @returns { string } Poprawnie sformatowany URL

  uri: function (suffix) {
    return ([civapp.conf.baseUrl, suffix]).join('');
  }
};

})($, _, kendo, civapp);