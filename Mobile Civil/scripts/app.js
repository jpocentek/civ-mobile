/**
 * app.js
 * ======
 *
 * Plik wejściowy całej aplikacji. Tutaj ładujemy wszystkie moduły i wywołujemy
 * główne funkcje cordova - łączymy API urządzenia z aplikacją.
 */

(function ($, cordova, civapp) {

"use strict";

var exec = cordova.require('cordova/exec');

var app = {
  initialize: function() {
    this.bindEvents();
  },

  bindEvents: function() {
    $(document).on('deviceready', this.onDeviceReady);
  },

  onDeviceReady: function() {
    app.receivedEvent('deviceready');
    civapp.initialize();
  },

  receivedEvent: function(id) {
    //console.log('Received Event: ' + id);
  }
};

app.initialize();

})(jQuery, cordova, civapp);
